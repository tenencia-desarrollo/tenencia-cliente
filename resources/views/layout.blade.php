<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Verificación - @yield('titulo')</title>
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
</head>
<body>
    <div class="container">
        <div class="row">
            <header>
                <h1>@yield('titulo')</h1>
            </header>
        </div>
        <!-- <div class="row"> -->
                <div class="col-md-4 col-md-offset-6">
                       
                                    @yield('contenido')
           
                </div>
        <!--</div>-->
    </div>
</body>
</html>