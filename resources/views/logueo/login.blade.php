@extends('layouts.base')

@section('content')
<div class="row">
    <label for="user"></label>
    <input type="text" name="user" id="user" placeholder="Type User">
    <label for="pass"></label>
    <input type="text" name="pass" id="pass" placeholder="Type Password">
    <button class="btn btn-primary" type="submit">Login</button>
</div>
@endsection