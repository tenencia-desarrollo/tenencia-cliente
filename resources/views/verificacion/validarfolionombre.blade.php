@extends('layout')
@section('titulo', 'Verificar folio del titular')
@section('contenido')


<form action="{{url('titularfolio')}}" method="get">
    
    <input class="form-control" name="nu_folio" placeholder="Folio RPP" pattern="[0-9]{1,10}" title="Ingrese solo números"  required >
    <input class="form-control" name="Nom_tit" placeholder="Nombre del titular" pattern="[A-Za-z]{3,40}" title="Ingrese solo texto" required >
     
    @if($respuesta != null)
        <p>{{$respuesta->status}}<p>     
    @else
        <p>  </p>       
    @endif
    
    <br><br><input class="btn btn-primary" type="submit" value="Verificar">
</form>

@endsection