<?php
  
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use PhpParser\Node\Stmt\TryCatch;

class VerificacionController extends Controller
{   

    private $cliente;

    public function __construct (){
        $this->cliente = new Client(['base_uri'=> 'http://localhost:8000/consultarpp/']);

    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
          $nu_folio = isset($_GET['nu_folio']) ? $_GET['nu_folio'] : '';
                $Nom_tit = isset($_GET['Nom_tit']) ? $_GET['Nom_tit'] : '';
                $cuerpo = null;
                if($nu_folio != '' || $Nom_tit != ''){
                    try {
                        
                        $respuestaServidor= $this->cliente->get('titularfolio/' .$nu_folio.'/'.$Nom_tit);
                        $cuerpo = $respuestaServidor->getBody();   
                    } catch (\Throwable $th) {
                        //throw $th;
                        $cuerpo=json_encode(['status'=> 'No hay comunicación con el servidor']);
                    }
                          
                }
                return view('verificacion.validarfolionombre', ['respuesta' => json_decode($cuerpo)]);
  // ^[A-Z][a-z][A-Z]
    }

}












/* 
INTENTO VALIDACION

public function show(Request $request)
    {

        $validar = $this->validate($request,[
            'nu_folio'=>'required|min:6|max:6|regex:/[0-9{6}]',
            'Nom_tit' => 'required|string|regex:/^[A-Z]/',
        ]);
        
        if($validar){

                $nu_folio = isset($_GET['nu_folio']) ? $_GET['nu_folio'] : '';
                $Nom_tit = isset($_GET['Nom_tit']) ? $_GET['Nom_tit'] : '';
                $cuerpo = null;
                if($nu_folio != '' || $Nom_tit != ''){
                        //code...
                        $respuestaServidor= $this->cliente->get('titularfolio/' .$nu_folio.'/'.$Nom_tit);
                        $cuerpo = $respuestaServidor->getBody();       
                }
                return view('verificacion.validarfolionombre', ['respuesta' => json_decode($cuerpo)]);
        }

        return back()->withErrors(['nu_folio','Nom_tit'])
        ->withInput(request(['nu_folio','Nom_tit']));
    }
*/
